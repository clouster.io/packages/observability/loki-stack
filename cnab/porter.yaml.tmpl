# This is the configuration for Porter
# You must define steps for each action, but the rest is optional
# See https://porter.sh/author-bundles for documentation on how to configure your bundle
# Uncomment out the sections below to take full advantage of what Porter can do!

name: loki-stack
version: "$CI_COMMIT_TAG"
description: "Loki: like Prometheus, but for logs."
registry: "$DOCKER_REPO_PROJECT"

dockerfile: Dockerfile.tmpl

mixins:
  - exec
  - helm3-centros

images:
  promtail:
    description: "Promtail service image"
    imageType: "docker"
    repository: "grafana/promtail"
    tag: "2.1.0"
  loki:
    description: "Loki service image"
    imageType: "docker"
    repository: "grafana/loki"
    tag: "2.4.2"

custom:
  "one.centros.csm":
    "$schema": https://gitlab.com/centros.one/composer/schemas/-/raw/0.2.0/csm-schema.json
    sockets:
      input: []
      output: []

credentials:
  - name: kubeconfig
    path: /root/.kube/config

parameters:
  - name: values
    path: /root/helm.values
    default: {}
    type: object
    properties:
      loki:
        properties:
          affinity:
            type: object
          alerting_groups:
            type: array
          annotations:
            type: object
          client:
            type: object
          config:
            properties:
              auth_enabled:
                type: boolean
              chunk_store_config:
                properties:
                  max_look_back_period:
                    type: string
                type: object
              compactor:
                properties:
                  shared_store:
                    type: string
                  working_directory:
                    type: string
                type: object
              ingester:
                properties:
                  chunk_block_size:
                    type: integer
                  chunk_idle_period:
                    type: string
                  chunk_retain_period:
                    type: string
                  lifecycler:
                    properties:
                      ring:
                        properties:
                          kvstore:
                            properties:
                              store:
                                type: string
                            type: object
                          replication_factor:
                            type: integer
                        type: object
                    type: object
                  max_transfer_retries:
                    type: integer
                  wal:
                    properties:
                      dir:
                        type: string
                    type: object
                type: object
              limits_config:
                properties:
                  enforce_metric_name:
                    type: boolean
                  reject_old_samples:
                    type: boolean
                  reject_old_samples_max_age:
                    type: string
                type: object
              ruler:
                properties:
                  alertmanager_url:
                    type: string
                  enable_api:
                    type: boolean
                  ring:
                    properties:
                      kvstore:
                        properties:
                          store:
                            type: string
                        type: object
                    type: object
                  rule_path:
                    type: string
                  storage:
                    properties:
                      local:
                        properties:
                          directory:
                            type: string
                        type: object
                      type:
                        type: string
                    type: object
                type: object
              schema_config:
                properties:
                  configs:
                    type: array
                type: object
              server:
                properties:
                  http_listen_port:
                    type: integer
                type: object
              storage_config:
                properties:
                  boltdb_shipper:
                    properties:
                      active_index_directory:
                        type: string
                      cache_location:
                        type: string
                      cache_ttl:
                        type: string
                      shared_store:
                        type: string
                    type: object
                  filesystem:
                    properties:
                      directory:
                        type: string
                    type: object
                type: object
              table_manager:
                properties:
                  retention_deletes_enabled:
                    type: boolean
                  retention_period:
                    type: string
                type: object
            type: object
          enabled:
            type: boolean
          env:
            type: array
          extraArgs:
            type: object
          extraContainers:
            type: array
          extraPorts:
            type: array
          extraVolumeMounts:
            type: array
          extraVolumes:
            type: array
          image:
            properties:
              digest:
                type: string
              pullPolicy:
                type: string
              pullSecrets:
                type: array
              repository:
                type: string
            type: object
          ingress:
            properties:
              annotations:
                type: object
              enabled:
                type: boolean
              hosts:
                type: array
              ingressClassName:
                type: string
              tls:
                type: array
            type: object
          initContainers:
            type: array
          livenessProbe:
            properties:
              httpGet:
                properties:
                  path:
                    type: string
                  port:
                    type: string
                type: object
              initialDelaySeconds:
                type: integer
            type: object
          networkPolicy:
            properties:
              enabled:
                type: boolean
            type: object
          nodeSelector:
            type: object
          persistence:
            properties:
              accessModes:
                type: array
              annotations:
                type: object
              enabled:
                type: boolean
              existingClaim:
                type: string
              selector:
                type: object
              size:
                type: string
              subPath:
                type: string
            type: object
          podAnnotations:
            properties:
              prometheus.io/port:
                type: string
              prometheus.io/scrape:
                type: string
            type: object
          podDisruptionBudget:
            type: object
          podLabels:
            type: object
          podManagementPolicy:
            type: string
          priorityClassName:
            type: string
          rbac:
            properties:
              create:
                type: boolean
              pspEnabled:
                type: boolean
            type: object
          readinessProbe:
            properties:
              httpGet:
                properties:
                  path:
                    type: string
                  port:
                    type: string
                type: object
              initialDelaySeconds:
                type: integer
            type: object
          replicas:
            type: integer
          resources:
            type: object
          securityContext:
            properties:
              fsGroup:
                type: integer
              runAsGroup:
                type: integer
              runAsNonRoot:
                type: boolean
              runAsUser:
                type: integer
            type: object
          service:
            properties:
              annotations:
                type: object
              labels:
                type: object
              nodePort:
                type: integer
              port:
                type: integer
              targetPort:
                type: string
              type:
                type: string
            type: object
          serviceAccount:
            properties:
              annotations:
                type: object
              automountServiceAccountToken:
                type: boolean
              create:
                type: boolean
              name:
                type: string
            type: object
          serviceMonitor:
            properties:
              additionalLabels:
                type: object
              annotations:
                type: object
              enabled:
                type: boolean
              interval:
                type: string
              path:
                type: string
              scrapeTimeout:
                type: string
            type: object
          terminationGracePeriodSeconds:
            type: integer
          tolerations:
            type: array
          tracing:
            properties:
              jaegerAgentHost:
                type: string
            type: object
          updateStrategy:
            properties:
              type:
                type: string
            type: object
        type: object
      promtail:
        properties:
          affinity:
            type: object
          annotations:
            type: object
          config:
            properties:
              client:
                properties:
                  backoff_config:
                    properties:
                      max_period:
                        type: string
                      max_retries:
                        type: integer
                      min_period:
                        type: string
                    type: object
                  batchsize:
                    type: integer
                  batchwait:
                    type: string
                  external_labels:
                    type: object
                  timeout:
                    type: string
                type: object
              positions:
                properties:
                  filename:
                    type: string
                type: object
              server:
                properties:
                  http_listen_port:
                    type: integer
                type: object
              target_config:
                properties:
                  sync_period:
                    type: string
                type: object
            type: object
          deploymentStrategy:
            type: object
          enabled:
            type: boolean
          env:
            type: array
          extraCommandlineArgs:
            type: array
          extraScrapeConfigs:
            type: array
          extraVolumeMounts:
            type: array
          extraVolumes:
            type: array
          image:
            properties:
              digest:
                type: string
              pullPolicy:
                type: string
              pullSecrets:
                type: array
              repository:
                type: string
            type: object
          initContainer:
            properties:
              enabled:
                type: boolean
              fsInotifyMaxUserInstances:
                type: integer
            type: object
          livenessProbe:
            type: object
          loki:
            properties:
              password:
                type: string
              serviceName:
                type: string
              servicePort:
                type: integer
              serviceScheme:
                type: string
              user:
                type: string
            type: object
          nameOverride:
            type: string
          nodeSelector:
            type: object
          pipelineStages:
            type: array
          podAnnotations:
            properties:
              prometheus.io/port:
                type: string
              prometheus.io/scrape:
                type: string
            type: object
          podLabels:
            type: object
          podSecurityPolicy:
            properties:
              allowPrivilegeEscalation:
                type: boolean
              fsGroup:
                properties:
                  rule:
                    type: string
                type: object
              hostIPC:
                type: boolean
              hostNetwork:
                type: boolean
              hostPID:
                type: boolean
              privileged:
                type: boolean
              readOnlyRootFilesystem:
                type: boolean
              requiredDropCapabilities:
                type: array
              runAsUser:
                properties:
                  rule:
                    type: string
                type: object
              seLinux:
                properties:
                  rule:
                    type: string
                type: object
              supplementalGroups:
                properties:
                  rule:
                    type: string
                type: object
              volumes:
                type: array
            type: object
          priorityClassName:
            type: string
          rbac:
            properties:
              create:
                type: boolean
              pspEnabled:
                type: boolean
            type: object
          readinessProbe:
            properties:
              failureThreshold:
                type: integer
              httpGet:
                properties:
                  path:
                    type: string
                  port:
                    type: string
                type: object
              initialDelaySeconds:
                type: integer
              periodSeconds:
                type: integer
              successThreshold:
                type: integer
              timeoutSeconds:
                type: integer
            type: object
          resources:
            type: object
          scrapeConfigs:
            type: array
          securityContext:
            properties:
              readOnlyRootFilesystem:
                type: boolean
              runAsGroup:
                type: integer
              runAsUser:
                type: integer
            type: object
          serviceAccount:
            properties:
              create:
                type: boolean
              name:
                type: string
            type: object
          serviceMonitor:
            properties:
              additionalLabels:
                type: object
              annotations:
                type: object
              enabled:
                type: boolean
              interval:
                type: string
            type: object
          syslogService:
            properties:
              annotations:
                type: object
              enabled:
                type: boolean
              externalIPs:
                type: array
              externalTrafficPolicy:
                type: string
              labels:
                type: object
              loadBalancerIP:
                type: string
              loadBalancerSourceRanges:
                type: array
              nodePort:
                type: integer
              port:
                type: integer
              type:
                type: string
            type: object
          tolerations:
            type: array
          volumeMounts:
            type: array
          volumes:
            type: array
        type: object
    applyTo:
      - install
      - upgrade
  - name: image-pull-secret
    type: string
    default: ""
    env: IMAGE_PULL_SECRET
    applyTo:
      - install
      - upgrade
  - name: namespace
    type: string
    default: default

install:
  - exec:
      description: "Prepare Helm chart"
      command: ./helpers.sh
      arguments:
        - prepare
        - ./helm
        - '"{{ bundle.name }}"'
        - '"{{ bundle.description }}"'
        - '"{{ bundle.version }}"'
  - exec:
      description: "Write image override values file"
      command: ./helpers.sh
      arguments:
        - write-values
        - /root/image.values
        - "loki.image.digest"
        - '"{{ bundle.images.loki.digest }}"'
        - "loki.image.repository"
        - '"{{ bundle.images.loki.repository }}"'
        - "promtail.image.digest"
        - '"{{ bundle.images.promtail.digest }}"'
        - "promtail.image.repository"
        - '"{{ bundle.images.promtail.repository }}"'
  - helm3-centros:
      description: "Install {{ bundle.name }}"
      name: "{{ installation.name }}"
      chart: "./helm"
      namespace: "{{ bundle.parameters.namespace }}"
      wait: true
      upsert: true
      idempotent: true
      values:
        - /root/image.values
        - /root/helm.values
        - /root/ips.values

upgrade:
  - exec:
      description: "Prepare Helm chart"
      command: ./helpers.sh
      arguments:
        - prepare
        - ./helm
        - '"{{ bundle.name }}"'
        - '"{{ bundle.description }}"'
        - '"{{ bundle.version }}"'
  - exec:
      description: "Write image override values file"
      command: ./helpers.sh
      arguments:
        - write-values
        - /root/image.values
        - "loki.image.digest"
        - '"{{ bundle.images.loki.digest }}"'
        - "loki.image.repository"
        - '"{{ bundle.images.loki.repository }}"'
        - "promtail.image.digest"
        - '"{{ bundle.images.promtail.digest }}"'
        - "promtail.image.repository"
        - '"{{ bundle.images.promtail.repository }}"'
  - helm3-centros:
      description: "Upgrade {{ bundle.name }}"
      name: "{{ installation.name }}"
      chart: "./helm"
      namespace: "{{ bundle.parameters.namespace }}"
      wait: true
      resetValues: true
      reuseValues: false
      values:
        - /root/image.values
        - /root/helm.values
        - /root/ips.values

uninstall:
  - helm3-centros:
      description: "Uninstall {{ bundle.name }}"
      namespace: "{{ bundle.parameters.namespace }}"
      releases:
        - "{{ installation.name }}"
